use master

go


create database DentalClinic2

go

use DentalClinic2


go
--create login onur with password  = 'onur'
/*create login mehmet with password = '123'
create login mehmet2 with password='123' must_change, check_expiration = on
GRANT CONTROL SERVER TO onur

GRANT ALTER ANY EVENT NOTIFICATION TO onur WITH GRANT OPTION;  
alter login onur enable
GRANT ALTER ANY DATABASE TO onur WITH GRANT OPTION ;
go*/


go
create schema Stocks

go

create schema Person

go

create schema Appointments

go

create schema Locations

go

create schema Diseases

go

create schema Financial

go

create schema Clinic

go


create table Locations.Country(
	
	CountryID int primary key identity(1,1),
	CountryName nvarchar(30) not null,
	CountryCode nvarchar(10) unique not null

)


go


create table Locations.[State](

	StateID int primary key identity(1,1),
	--CountryID int foreign key references Locations.Country(CountryID),
	StateName nvarchar(30) unique not null

)

go

create table Locations.City(
	
	CityID int primary key identity(1,1),
	--CountryID int foreign key references Locations.Country(CountryID),
	--StateID int foreign key references Locations.[State](StateID),
	CityName nvarchar(30) unique not null

)

go

create table Locations.[Address](
	
	AddressID int primary key identity(1,1),	
	CountryID int foreign key references Locations.Country(CountryID),
	StateID int foreign key references Locations.[State](StateID),
	CityID int foreign key references Locations.City(CityID),
	[FullAddress] nvarchar(100) not null
)

go


create table Stocks.DrugType(

	DrugTypeID int primary key identity(1,1),
	DrugName nvarchar(30) not null,

)

go

create table Stocks.Brand(
	
	BrandID int primary key identity(1,1),
	BrandName nvarchar(30) not null,
	AddressID int foreign key references Locations.[Address](AddressID),
	PhoneNumber nvarchar(20) not null,
	Constraint chk_phone2 check (PhoneNumber like '[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]'),
	Email nvarchar(50) not null,
	
)

go

create table Stocks.Supplier(
	
	SupplierID int primary key identity(1,1),
	SupplierName nvarchar(30) not null,
	AddressID int foreign key references Locations.[Address](AddressID),
	PhoneNumber nvarchar(20) not null,
	Constraint chk_phone1 check (PhoneNumber like '[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]'),
	Email nvarchar(50) not null

)

go

create table Stocks.Drug(

	DrugID int primary key identity(1,1),
	DrugTypeID int foreign key references Stocks.DrugType(DrugTypeID),
	BrandID int foreign key references Stocks.Brand(BrandID),
	SupplierID int foreign key references Stocks.Supplier(SupplierID),
	ExpDate datetime not null,
	AcquisitionDate datetime default(getdate()),
	IsUsed bit default(0)
)

go


create table Clinic.Department(

	DepartmentID int primary key identity(1,1),
	DepartmentName nvarchar(30) not null,
	AddressID int foreign key references Locations.[Address](AddressID),
	Phone nvarchar(20) not null
	--ManagerID int foreign key references Person.Employee(EmployeeID)
)

go
create table Person.Employee(

	EmployeeID int primary key identity(1,1),
	SSN nvarchar(20) not null unique,
	EmployeeFirstName nvarchar(30) not null,
	EmployeeMiddleName nvarchar(30),
	EmployeeLastName nvarchar(30) not null,
	AddressID int foreign key references Locations.[Address](AddressID),
	BirthDate datetime not null,
	PhoneNumber nvarchar(20) not null ,
	Constraint chk_phone3 check (PhoneNumber like '[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]'),
	DepartmentID int foreign key references Clinic.Department(DepartmentID),
	Email nvarchar(50) not null,
	NationalityID int foreign key references Locations.Country(CountryID),
	Recuitment datetime not null,
	Salary decimal(8,2) not null,
	IsWorking bit default(1)
)

go

create table Clinic.Substation(
	SubstationID int primary key identity(1,1),
	SubstationName nvarchar(30) not null unique,
	LocationID int foreign key references Locations.[Address](AddressID),
	DirectorID int foreign key references Person.Employee(EmployeeID),
	PhoneNumber nvarchar(20) not null,
	Constraint chk_phone4 check (PhoneNumber like '[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]')
)

go

create table Person.Patient(
	PatientID int primary key identity(1,1),
	SSN nvarchar(20) not null unique,
	PatientFirstName nvarchar(30) not null,
	PatientMiddleName nvarchar(30),
	PatientLastName nvarchar(30) not null,
	AddressID int foreign key references Locations.[Address](AddressID),
	BirthDate datetime not null,
	PhoneNumber nvarchar(20) not null,
	Constraint chk_phone5 check (PhoneNumber like '[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]'),
	Email nvarchar(50)
)


create table Appointments.Prescriptions(
	PrescriptionID int primary key identity(1,1),
	PatientID int foreign key references Person.Patient(PatientID),
	Drugs nvarchar(100) not null,
	PrescriptionDate datetime,
	EmployeeID int foreign key references Person.Employee(EmployeeID)
)
go

create table Stocks.EquipmentType(

	EquipmentTypeID int primary key identity(1,1),
	EquipmentName nvarchar(30) not null,
)

go

create table Stocks.MedicalEquipment(

	EquipmentID int primary key identity(1,1),
	DepartmentID int foreign key references Clinic.Department(DepartmentID),
	EquipmentTypeID int foreign key references Stocks.EquipmentType(EquipmentTypeID),
	BrandID int foreign key references Stocks.Brand(BrandID),
	SupplierID int foreign key references Stocks.Supplier(SupplierID),
	ExpDate datetime not null,
	AcquisitionDate datetime default(getdate()),
	IsUsed bit default(0)

)
go

create table Appointments.Appointment(
	AppointmentID int primary key identity(1,1),
	AppointmentDate datetime,
	AppointmentFlag bit,
	DepartmentID int foreign key references Clinic.Department(DepartmentID),
	EmployeeID int foreign key references Person.Employee(EmployeeID),
	DiseaseName nvarchar(30),
	PrescriptionID int foreign key references Appointments.Prescriptions(PrescriptionID),
	PatientID int foreign key references Person.Patient(PatientID),
	LocationID int foreign key references Locations.[Address](AddressID),
	Explanation nvarchar(200),
)
go

create table Financial.Treatment(
	TreatmentID int primary key identity(1,1),
	TreatmentName nvarchar(30) not null,
	Explanation nvarchar(100),
	Cost decimal(8,2) not null,
	DepartmentID int foreign key references Clinic.Department(DepartmentID)
)
go

create table Financial.Income(
	Explanation nvarchar(200),
	Quantity Decimal(8,2) not null,
	[Date] datetime not null,
	DeparmentID int foreign key references Clinic.Department(DepartmentID)
)
go
create table Financial.Expense(
	Explanation nvarchar(200),
	Quantity decimal(8,2) not null,
	[Date] datetime not null,
	DeparmentID int foreign key references Clinic.Department(DepartmentID)
)
go

alter table Locations.[State] add CountryID int foreign key references Locations.Country(CountryID)
alter table Locations.City add CountryID int foreign key references Locations.Country(CountryID)
alter table Locations.City add StateID int foreign key references Locations.[State](StateID)

go

INSERT INTO Locations.Country (CountryName,CountryCode) VALUES ('Georgia', 'GEO');
INSERT INTO Locations.Country (CountryName,CountryCode) VALUES ('Germany (Deutschland)', 'DEU');
INSERT INTO Locations.Country (CountryName,CountryCode) VALUES ('Greece', 'GRC');
INSERT INTO Locations.Country (CountryName,CountryCode) VALUES ('Russian Federation', 'RUS');
INSERT INTO Locations.Country (CountryName,CountryCode) VALUES ('Spain (espa�a)', 'ESP');
INSERT INTO Locations.Country (CountryName,CountryCode) VALUES ('Sweden', 'SWE');
INSERT INTO Locations.Country (CountryName,CountryCode) VALUES ('Turkey', 'TUR');
INSERT INTO Locations.Country (CountryName,CountryCode) VALUES ('United Kingdom (Great Britain)', 'GBR');
INSERT INTO Locations.Country (CountryName,CountryCode) VALUES ('United States', 'USA');

go
--ABD
INSERT into Locations.[State] values 
(N'Alabama',9),
(N'Alaska',9),
(N'Arizona',9),
(N'Arkansas',9),
(N'California',9),
(N'Colorado',9),
(N'Connecticut',9),
(N'Delaware',9),
(N'District of Columbia',9),
(N'Florida',9),
(N'Georgia',9),
(N'Hawaii',9),
(N'Idaho',9),
(N'Illinois',9),
(N'Indiana',9),
(N'Iowa',9),
(N'Kansas',9),
(N'Kentucky',9),
(N'Louisiana',9),
(N'Maine',9),
(N'Maryland',9),
(N'Massachusetts',9),
(N'Michigan',9),
(N'Minnesota',9),
(N'Mississippi',9),
(N'Missouri',9),
(N'Montana',9),
(N'Nebraska',9),
(N'Nevada',9),
(N'New Hampshire',9),
(N'New Jersey',9),
(N'New Mexico',9),
(N'New York',9),
(N'North Carolina',9),
(N'North Dakota',9),
(N'Ohio',9),
(N'Oklahoma',9),
(N'Oregon',9),
(N'Pennsylvania',9),
(N'Puerto Rico',9),
(N'Rhode Island',9),
(N'South Carolina',9),
(N'South Dakota',9),
(N'Tennessee',9),
(N'Texas',9),
(N'Utah',9),
(N'Vermont',9),
(N'Virginia',9),
(N'Washington',9),
(N'West Virginia',9),
(N'Wisconsin',9),
(N'Wyoming',9);

go
--Turkey

insert into Locations.[State] values 
('Adana', 7),
('Adiyaman', 7),
('Afyon',7),
('Agri', 7),
('Aksaray', 7),
('Amasya', 7),
('Ankara', 7),
('Antalya', 7),
('Ardahan', 7),
('Artvin', 7),
('Aydin', 7),
('Balikesir', 7),
('Bartin', 7),
('Batman', 7),
('Bayburt', 7),
('Bilecik', 7),
('Bingol', 7),
('Bitlis', 7),
('Bolu', 7),
('Burdur', 7),
('Bursa', 7),
('Canakkale', 7),
('Cankiri', 7),
('Corum', 7),
('Denizli', 7),
('Diyarbakir',7),
('Duzce', 7),
('Edirne', 7),
('Elazig', 7),
('Erzincan', 7),
('Erzurum', 7),
('Eskisehir', 7),
('Gaziantep', 7),
('Giresun', 7),
('Gumushane', 7),
('Hakkari', 7),
('Hatay', 7),
('Icel', 7),
('Igdir', 7),
('Isparta', 7),
('Istanbul', 7),
('Izmir', 7),
('Kahramanmaras', 7),
('Karabuk', 7),
('Karaman', 7),
('Kars', 7),
('Karsiyaka', 7),
('Kastamonu', 7),
('Kayseri', 7),
('Kilis', 7),
('Kirikkale', 7),
('Kirklareli', 7),
('Kirsehir', 7),
('Kocaeli', 7),
('Konya', 7),
('Kutahya', 7),
('Lefkosa', 7),
('Malatya', 7),
('Manisa', 7),
('Mardin', 7),
('Mugla', 7),
('Mus', 7),
('Nevsehir', 7),
('Nigde', 7),
('Ordu', 7),
('Osmaniye', 7),
('Rize', 7),
('Sakarya', 7),
('Samsun', 7),
('Sanliurfa', 7),
('Siirt', 7),
('Sinop', 7),
('Sirnak', 7),
('Sivas', 7),
('Tekirdag', 7),
('Tokat', 7),
('Trabzon', 7),
('Tunceli', 7),
('Usak', 7),
('Van', 7),
('Yalova', 7),
('Yozgat', 7),
('Zonguldak', 7)

--Turkey cities
go

insert into Locations.City values 
('Adana', 7,53),
('Adiyaman', 7,54),
('Afyon',7,55),
('Agri', 7,56),
('Aksaray', 7,57),
('Amasya', 7,58),
('Ankara', 7,59),
('Antalya', 7,60),
('Ardahan', 7,61),
('Artvin', 7,62),
('Aydin', 7,63),
('Balikesir', 7,64),
('Bartin', 7,65),
('Batman', 7,66),
('Bayburt', 7,67),
('Bilecik', 7,68),
('Bingol', 7,69),
('Bitlis', 7,70),
('Bolu', 7,71),
('Burdur', 7,72),
('Bursa', 7,73),
('Canakkale', 7,74),
('Cankiri', 7,75),
('Corum', 7,76),
('Denizli', 7,77),
('Diyarbakir',7,78),
('Duzce', 7,79),
('Edirne', 7,80),
('Elazig', 7,81),
('Erzincan', 7,82),
('Erzurum', 7,83),
('Eskisehir', 7,84),
('Gaziantep', 7,85),
('Giresun', 7,86),
('Gumushane', 7,87),
('Hakkari', 7,88),
('Hatay', 7,89),
('Icel', 7,90),
('Igdir', 7,91),
('Isparta', 7,92),
('Istanbul', 7,93),
('Izmir', 7,94),
('Kahramanmaras', 7,95),
('Karabuk', 7,96),
('Karaman', 7,97),
('Kars', 7,98),
('Karsiyaka', 7,99),
('Kastamonu', 7,100),
('Kayseri', 7,101),
('Kilis', 7,102),
('Kirikkale', 7,103),
('Kirklareli', 7,104),
('Kirsehir', 7,105),
('Kocaeli', 7,106),
('Konya', 7,107),
('Kutahya', 7,108),
('Lefkosa', 7,109),
('Malatya', 7,110),
('Manisa', 7,111),
('Mardin', 7,112),
('Mugla', 7,113),
('Mus', 7,114),
('Nevsehir', 7,115),
('Nigde', 7,116),
('Ordu', 7,117),
('Osmaniye', 7,118),
('Rize', 7,119),
('Sakarya', 7,120),
('Samsun', 7,121),
('Sanliurfa', 7,122),
('Siirt', 7,123),
('Sinop', 7,124),
('Sirnak', 7,125),
('Sivas', 7,126),
('Tekirdag', 7,127),
('Tokat', 7,128),
('Trabzon', 7,129),
('Tunceli', 7,130),
('Usak', 7,131),
('Van', 7,132),
('Yalova', 7,133),
('Yozgat', 7,134),
('Zonguldak', 7,135)

go

insert into Locations.[Address] values (7,93,41,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

insert into Locations.[Address] values (7,61,9,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

insert into Locations.[Address] values (7,60,8,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

insert into Locations.[Address] values (7,59,7,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

insert into Locations.[Address] values (7,58,6,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

insert into Locations.[Address] values (7,57,5,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

insert into Locations.[Address] values (7,56,4,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

insert into Locations.[Address] values (7,55,3,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

insert into Locations.[Address] values (7,54,2,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

insert into Locations.[Address] values (7,53,1,N'�evketiye Mahallesi arslan sokak Asl� apartman� no:14 daire:6 ')

go

declare @w int
set @w = 1
while @w < 5
begin
insert into [Clinic].[Department] values (N'Enamelplasty', 1 ,N'212-444-54-44')

insert into [Clinic].[Department] values (N'Orthodontics', 1 ,N'212-444-54-43')

insert into [Clinic].[Department] values (N'Implantology', 1 ,N'212-444-54-42')

insert into [Clinic].[Department] values (N'Denture', 3 ,N'212-444-54-41')

insert into [Clinic].[Department] values (N'Bleaching', 2 ,N'212-444-44-40')

insert into [Clinic].[Department] values (N'Cons. Odontotherapy', 4 ,N'212-444-44-39')

insert into [Clinic].[Department] values (N'Endodontics', 5 ,N'212-444-54-38')

insert into [Clinic].[Department] values (N'Periodontology', 2 ,N'212-444-54-37')

insert into [Clinic].[Department] values (N'Oral Diagnosis and Radiology ', 1 ,N'212-444-44-36')

set @w = @w + 1

end

go
/*
select * from Locations.Country

insert into Person.Employee values (N'53452137456',N'Erkan',null,N'Solmaz',6,GETDATE(),3000,N'212-567-34-67',9,N'erkan.solmaz@gmail.com',7,GETDATE())

insert into Person.Employee values (N'53452137456',N'Erkan',null,N'Solmaz',6,GETDATE(),3000,N'212-567-34-67',9,N'erkan.solmaz@gmail.com',7,GETDATE())

insert into Person.Employee values (N'53452137456',N'Erkan',null,N'Solmaz',6,GETDATE(),3000,N'212-567-34-67',9,N'erkan.solmaz@gmail.com',7,GETDATE())

insert into Person.Employee values (N'53452137456',N'Erkan',null,N'Solmaz',6,GETDATE(),3000,N'212-567-34-67',9,N'erkan.solmaz@gmail.com',7,GETDATE())

go
*/

go
insert into Person.Employee values 
(N'53452137453',
N'Onur',
null,
N'�anl�',
6,
GETDATE(),
N'212-567-34-70',
9,
N'onur.sanli@dentalclinic.com',
7,
GETDATE(),
3000,
default
)
go
insert into Person.Employee values 
(N'53452137456',
N'Erkan',
null,
N'Solmaz',
6,
GETDATE(),
N'212-567-34-67',
9,
N'erkan.solmaz@gmail.com',
7,
GETDATE(),
3000,
default
)
go
insert into Person.Employee values (N'53452137454',
N'�afak',
null,
N'�an',
5,
GETDATE(),

N'212-567-34-67',
9,
N'safak.san@@dentalclinic.com',
7,
GETDATE(),
2500,
default
)
go
insert into Person.Employee values 
(N'53452137412',
N'Onur',
null,
N'Yana��k',
4,
GETDATE(),

N'212-567-34-67',
9,
N'onur.yanasik@dentalclinic.com',
7,
GETDATE(),
2000,
default
)
go
insert into Person.Employee values 
(N'53452137452',
N'Hilal',
null,
N'Do�an',
3,
GETDATE(),
N'212-567-34-67',
9,
N'hilal.dogan@dentalclinic.com',
7,
GETDATE(),
1500,
default
)
go
insert into Person.Employee values 
(N'53452137451',
N'Tu�ba',
null,
N'G�ne�',
2,
GETDATE(),
N'212-567-34-67',
9,
N'tugba.gunes@dentalclinic.com',
7,
GETDATE(),
1500,
default
)
go
insert into Person.Employee values 
(N'53452137457',
N'Kaan',
null,
N'Ece',
1,
GETDATE(),
N'212-567-34-67',
9,
N'kaan.ece@dentalclinic.com',
7,
GETDATE(),
1500,
default
)
go
insert into Person.Employee values 
(N'53452137459',
N'Berk',
null,
N'Dindaro�lu',
7,
GETDATE(),
N'212-567-34-67',
9,
N'berk.dindaroglu@dentalclinic.com',
7,
GETDATE(),
3000,
default
)
go
insert into Person.Employee values 
(N'53452137450',
N'Fahri',
N'Emre',
N'�zdemir',
1,
GETDATE(),
N'212-567-34-67',
9,
N'fahri.ozdemir@dentalclinic.com',
7,
GETDATE(),
1500,
default
)
go
insert into Person.Employee values 
(N'53452137413',
N'Mehmet',
N'Fatih',
N'�zdemir',
4,
GETDATE(),
N'212-567-34-67',
9,
N'mehmet.ozdemir@dentalclinic.com',
7,
GETDATE(),
31000,
default
)
go
insert into Person.Employee values 
(N'53452137415',
N'Selin',
null,
N'Aktar',
6,
GETDATE(),
N'212-567-34-67',
9,
N'selin.aktar@dentalclinic.com',
7,
GETDATE(),
3000,
default
)

go

alter table Clinic.Department add ManagerID int foreign key references Person.Employee(EmployeeID)
alter table Clinic.Department add SubstationID int foreign key references Clinic.Substation(SubstationID)
go

Update Clinic.Department set ManagerID = 1 where DepartmentID = 1
Update Clinic.Department set ManagerID = 2 where DepartmentID = 2
Update Clinic.Department set ManagerID = 3 where DepartmentID = 3
Update Clinic.Department set ManagerID = 4 where DepartmentID = 4
Update Clinic.Department set ManagerID = 5 where DepartmentID = 5
Update Clinic.Department set ManagerID = 6 where DepartmentID = 6
Update Clinic.Department set ManagerID = 7 where DepartmentID = 7
Update Clinic.Department set ManagerID = 8 where DepartmentID = 8
Update Clinic.Department set ManagerID = 9 where DepartmentID = 9

go

insert into Person.Patient values 
(
	N'15478562451',
	N'Onur',
	null,
	N'�anlee',
	4,
	GETDATE(),
	N'212-567-34-67',
	N'onur.sanli@dentalclinic.com'
)

insert into Person.Patient values 
(
	N'15478562452',
	N'Tu�ba',
	null,
	N'G�ne�',
	4,
	GETDATE(),
	N'212-567-34-68',
	N'tugba.gunes@dentalclinic.com'
)

insert into Person.Patient values 
(
	N'15478562453',
	N'Mehmet',
	N'Fatih',
	N'�zdemir',
	4,
	GETDATE(),
	N'212-567-34-69',
	N'mehmet.ozdemir@dentalclinic.com'
)

insert into Person.Patient values 
(
	N'15478562454',
	N'Fahri',
	N'Emre',
	N'�zdemir',
	4,
	GETDATE(),
	N'212-567-34-70',
	N'fahri.ozdemir@dentalclinic.com'
)

insert into Person.Patient values 
(
	N'15478562455',
	N'Hilal',
	null,
	N'Do�an',
	4,
	GETDATE(),
	N'212-567-34-71',
	N'hilal.dogan@dentalclinic.com'
)

insert into Person.Patient values 
(
	N'15478562456',
	N'Berk',
	null,
	N'Dindaro�lu',
	4,
	GETDATE(),
	N'212-567-34-72',
	N'berk.dindaroglu@dentalclinic.com'
)
insert into Person.Patient values 
(
	N'15478562457',
	N'Onur',
	null,
	N'Yana��k',
	4,
	GETDATE(),
	N'212-567-34-73',
	N'onur.yanasik@dentalclinic.com'
)
insert into Person.Patient values 
(
	N'15478562458',
	N'�afak',
	null,
	N'�an',
	4,
	GETDATE(),
	N'212-567-34-74',
	N'safak.san@dentalclinic.com'
)
insert into Person.Patient values 
(
	N'15478562459',
	N'Alperen',
	null,
	N'Yanc�',
	4,
	GETDATE(),
	N'212-567-34-75',
	N'alperen.yanci@dentalclinic.com'
)
insert into Person.Patient values 
(
	N'15478562460',
	N'Erkan',
	null,
	N'Solmaz',
	4,
	GETDATE(),
	N'212-567-34-76',
	N'erkan.solmaz@dentalclinic.com'
)
insert into Person.Patient values 
(
	N'15478562461',
	N'Kaan',
	null,
	N'Ece',
	4,
	GETDATE(),
	N'212-567-34-77',
	N'kaan.ece@dentalclinic.com'
)
go

insert into Appointments.Prescriptions values 
(
	1,
	N'Apranax, Augmentin',
	GETDATE(),
	1
)

insert into Appointments.Prescriptions values 
(
	2,
	N'Apranax, Augmentin, Klamoks',
	GETDATE(),
	2
)

insert into Appointments.Prescriptions values 
(
	3,
	N'Klamoks, Apranax, Augmentin',
	GETDATE(),
	3
)
insert into Appointments.Prescriptions values 
(
	4,
	N'Augmentin',
	GETDATE(),
	4
)
insert into Appointments.Prescriptions values 
(
	5,
	N'Apranax',
	GETDATE(),
	5
)
insert into Appointments.Prescriptions values 
(
	6,
	N'Apranax, Augmentin',
	GETDATE(),
	6
)
go

insert into Appointments.Appointment values 
(
	GETDATE(),
	0,
	1,
	1,
	N'Berk',
	1,
	1,
	4,
	N'Kanal Tedavisi'
)

insert into Appointments.Appointment values 
(
	GETDATE(),
	1,
	1,
	2,
	N'Berk',
	2,
	2,
	4,
	N'Kanal Tedavisi'
)
insert into Appointments.Appointment values 
(
	GETDATE(),
	0,
	3,
	3,
	N'Onur',
	3,
	3,
	4,
	N'Kanal Tedavisi'
)
insert into Appointments.Appointment values 
(
	GETDATE(),
	1,
	5,
	4,
	N'�anlee',
	5,
	5,
	4,
	N'Kanal Tedavisi'
)

insert into Appointments.Appointment values 
(
	GETDATE(),
	0,
	6,
	6,
	N'Berk',
	6,
	6,
	4,
	N'Kanal Tedavisi'
)
insert into Appointments.Appointment values 
(
	GETDATE(),
	1,
	7,
	7,
	N'Berk',
	1,
	7,
	4,
	N'Kanal Tedavisi'
)
insert into Appointments.Appointment values 
(
	GETDATE(),
	0,
	2,
	4,
	N'Berk',
	1,
	8,
	4,
	N'Kanal Tedavisi'
)

go

insert into Clinic.Substation values 
(
	N'Bah�elievler',
	4,
	1,
	N'212-567-34-77'
)

insert into Clinic.Substation values 
(
	N'Bak�rk�y',
	4,
	2,
	N'212-567-34-78'
)

insert into Clinic.Substation values 
(
	N'Ba�ak�ehir',
	4,
	3,
	N'212-567-34-79'
)

insert into Clinic.Substation values 
(
	N'Ba�c�lar',
	4,
	4,
	N'212-567-34-80'
)

insert into Clinic.Substation values 
(
	N'Be�ikta�',
	4,
	5,
	N'212-567-34-81'
)

insert into Clinic.Substation values 
(
	N'Esenler',
	4,
	6,
	N'212-567-34-82'
)

insert into Clinic.Substation values 
(
	N'G�ng�ren',
	4,
	7,
	N'212-567-34-83'
)
go

insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
insert into Financial.Income values 
(
	N'Exemination Fee',
	15.00,
	getdate(),
	1
)
go

insert into Financial.Expense values 
(
	N'Syringe Cost',
	10.00,
	getdate(),
	2
)

insert into Financial.Expense values 
(
	N'Syringe Cost',
	10.00,
	getdate(),
	2
)

insert into Financial.Expense values 
(
	N'Syringe Cost',
	10.00,
	getdate(),
	2
)

insert into Financial.Expense values 
(
	N'Syringe Cost',
	10.00,
	getdate(),
	2
)
insert into Financial.Expense values 
(
	N'Syringe Cost',
	10.00,
	getdate(),
	2
)
insert into Financial.Expense values 
(
	N'Syringe Cost',
	10.00,
	getdate(),
	2
)
insert into Financial.Expense values 
(
	N'Syringe Cost',
	10.00,
	getdate(),
	2
)
insert into Financial.Expense values 
(
	N'Syringe Cost',
	10.00,
	getdate(),
	2
)

go

--alter table Financial.Treatment add DepartmentID int foreign key references Clinic.Department(DepartmentID)

declare @n int
set @n = 0
while @n<4
begin
insert into Financial.Treatment values 
(
	N'Canal Treatment',
	N'Standart Canal Treatment',
	50.00,
	@n*9+1
	
)

insert into Financial.Treatment values 
(
	N'Exodontia',
	N'Standart Exodontia',
	40.00,
	@n*9+2
)

insert into Financial.Treatment values 
(
	N'Implant',
	N'Standart Implant',
	60.00,
	@n*9+3
)

insert into Financial.Treatment values 
(
	N'Dental Braces',
	N'',
	100.35,
	@n*9+4
)

insert into Financial.Treatment values 
(
	N'Filling',
	N'',
	70.55,
	@n*9+5
)

set @n = @n +1
end
go

insert into Stocks.Brand values 
(
	N'Eczac�ba��',
	5,
	N'212-567-34-85',
	N'iletisim@eczacibasi.com'
)

insert into Stocks.Brand values 
(
	N'Sandoz',
	5,
	N'212-567-34-86',
	N'iletisim@sandoz.com'
)

insert into Stocks.Brand values 
(
	N'Abdi Ibrahim',
	6,
	N'212-567-34-87',
	N'iletisim@abdiibrahim.com'
)

insert into Stocks.Brand values 
(
	N'Actavis',
	2,
	N'212-567-34-88',
	N'iletisim@actavis.com'
)

insert into Stocks.Brand values 
(
	N'Haver',
	5,
	N'212-567-34-90',
	N'iletisim@haver.com'
)
go

insert into Stocks.DrugType values 
(
	N'Augmentin'
)

insert into Stocks.DrugType values 
(
	N'Apranax'
)

insert into Stocks.DrugType values 
(
	N'Minoset'
)

insert into Stocks.DrugType values 
(
	N'Klamoks'
)

insert into Stocks.DrugType values 
(
	N'Klindan'
)

insert into Stocks.DrugType values 
(
	N'Majezik'
)

insert into Stocks.DrugType values 
(
	N'Klavunat'
)
go

insert into Stocks.Supplier values 
(
	N'Helvac�zade',
	1,
	N'212-567-34-91',
	N'iletisim@helvacizade.com'
)

insert into Stocks.Supplier values 
(
	N'Hakay',
	2,
	N'212-567-34-92',
	N'iletisim@hakay.com'
)

insert into Stocks.Supplier values 
(
	N'Hero',
	3,
	N'212-567-34-93',
	N'iletisim@hero.com'
)

insert into Stocks.Supplier values 
(
	N'Helba',
	4,
	N'212-567-34-94',
	N'iletisim@helba.com'
)

insert into Stocks.Supplier values 
(
	N'Magna Pharma',
	1,
	N'212-567-34-95',
	N'iletisim@magnapharma.com'
)

insert into Stocks.Supplier values 
(
	N'Mecom',
	1,
	N'212-567-34-96',
	N'iletisim@mecom.com'
)

insert into Stocks.Supplier values 
(
	N'Mamsel',
	2,
	N'212-567-34-97',
	N'iletisim@mamsel.com'
)

insert into Stocks.Supplier values 
(
	N'Mefar',
	1,
	N'212-567-34-98',
	N'iletisim@mefar.com'
)
go
insert into Stocks.Drug values 
(
	1,
	1,
	1,
	GETDATE(),
	getdate(),
	0
)
insert into Stocks.Drug values 
(
	1,
	1,
	2,
	GETDATE(),
	getdate(),
	0
)
insert into Stocks.Drug values 
(
	1,
	2,
	1,
	GETDATE(),
	getdate(),
	1
)
insert into Stocks.Drug values 
(
	2,
	1,
	1,
	GETDATE(),
	getdate(),
	0
)
insert into Stocks.Drug values 
(
	2,
	1,
	2,
	GETDATE(),
	getdate(),
	1
)
insert into Stocks.Drug values 
(
	2,
	2,
	1,
	GETDATE(),
	getdate(),
	0
)
insert into Stocks.Drug values 
(
	1,
	2,
	4,
	GETDATE(),
	getdate(),
	0
)
insert into Stocks.Drug values 
(
	1,
	3,
	5,
	GETDATE(),
	getdate(),
	1
)
insert into Stocks.Drug values 
(
	2,
	5,
	3,
	GETDATE(),
	getdate(),
	1
)
insert into Stocks.Drug values 
(
	2,
	5,
	5,
	GETDATE(),
	getdate(),
	0
)
insert into Stocks.Drug values 
(
	2,
	4,
	3,
	GETDATE(),
	getdate(),
	1
)

go

insert into Stocks.EquipmentType values
(
	N'Scrub'
)
insert into Stocks.EquipmentType values
(
	N'Syringe'
)
insert into Stocks.EquipmentType values
(
	N'Filler'
)

go

go

declare @num integer
set @num=1
while @num < 9
begin
insert into Stocks.MedicalEquipment values 
(
	@num,
	1,
	1,
	1,
	getdate(),
	getdate(),
	0
)
set @num=@num+1
end
go

declare @nume integer
set @nume=1
while @nume < 9
begin
insert into Stocks.MedicalEquipment values 
(
	@nume,
	1,
	5,
	4,
	getdate(),
	getdate(),
	0
)
set @nume=@nume+1
end
go

declare @numee integer
set @numee=1
while @numee < 9
begin
insert into Stocks.MedicalEquipment values 
(
	@numee,
	1,
	2,
	3,
	getdate(),
	getdate(),
	1
)
set @numee=@numee+1
end
go

declare @n int
set @n = 0
while @n < 5
begin

update Clinic.Department set SubstationID = @n+1 where DepartmentID = 9*@n+1
update Clinic.Department set SubstationID = @n+1 where DepartmentID = 9*@n+2
update Clinic.Department set SubstationID = @n+1 where DepartmentID = 9*@n+3
update Clinic.Department set SubstationID = @n+1 where DepartmentID = 9*@n+4
update Clinic.Department set SubstationID = @n+1 where DepartmentID = 9*@n+5
update Clinic.Department set SubstationID = @n+1 where DepartmentID = 9*@n+6
update Clinic.Department set SubstationID = @n+1 where DepartmentID = 9*@n+7
update Clinic.Department set SubstationID = @n+1 where DepartmentID = 9*@n+8
update Clinic.Department set SubstationID = @n+1 where DepartmentID = 9*@n+9

set @n = @n + 1
end
go
create procedure Appointments.selectAppointment (@PID int) as 
begin

update Appointments.Appointment set AppointmentFlag = 1 where AppointmentDate < GETDATE()
update Appointments.Appointment set AppointmentFlag = 0 where AppointmentDate >= GETDATE()
select * from Appointments.Appointment
end

go

create procedure Appointments.addAppointment (@Date as datetime, @DID as int, @EID as int, @PID as int, @LID as int, @Exp as Nvarchar(200) ) as 
begin
insert into Appointments.Appointment values 
(
	@Date,
	0,
	@DID,
	@EID,
	null,
	null,
	@PID,
	@LID,
	@Exp
)

end

 go

create procedure Appointments.addPrescriptions (@PID as int, @Drug as nvarchar(100), @EID as int) as 
begin

insert into Appointments.Prescriptions values
(
	@PID,
	@Drug,
	getdate(),
	@EID
)

end

go

create procedure Appointments.addDepartment (@Name as nvarchar(30), @AID as int, @Pho as nvarchar(20), @MID as int, @SID as int) as
begin

insert into Clinic.Department values 
(
	@Name,
	@AID,
	@Pho,
	@MID,
	@SID
)

end

go

create procedure Appointments.addSubstation (@Name as nvarchar(30), @LID as int, @DID as int, @Pho as nvarchar(20)) as 
begin

insert into Clinic.Substation values 
(
	@Name,
	@LID,
	@DID,
	@Pho
)

end
go

create procedure Appointments.addExpense (@Exp as nvarchar(200), @Quan as decimal(8,2), @DID as int) as 
begin

insert into Financial.Expense values 
(
	@Exp,
	@Quan,
	getdate(),
	@DID
)

end

go

create procedure Appointments.addIncome (@Exp as nvarchar(200), @Quan as decimal(8,2), @DID as int) as
begin 

insert into Financial.Income values
(
	@Exp,
	@Quan,
	getdate(),
	@DID
)

end
go

create procedure Appointments.addTreatment (@Name as nvarchar(30),@Exp as nvarchar(100),@Cost as decimal(8,2), @DepartmentID int) as 
begin
insert into Financial.Treatment values
(
	@Name,
	@Exp,
	@Cost,
	@DepartmentID

)
end

go

create procedure Appointments.addAddress (@CID as int, @SID as int, @CityID as int, @FA as nvarchar(100)) as
begin

insert into Locations.[Address] values
(
	@CID,
	@SID,
	@CityID,
	@FA
)

end

go

create procedure Appointments.addCity (@Name as nvarchar(30),@CID as int, @SID as int) as
begin

insert into Locations.City values 
(
	@Name,
	@CID,
	@SID
)

end

go

create procedure Appointments.addCountry (@Name as nvarchar(30), @CC as nvarchar(10)) as 
begin 

insert into Locations.Country values 
(
	@Name,
	@CC
)
end

go

create procedure Appointments.addState (@Name as nvarchar(30), @CID as int) as
begin 

insert into Locations.[State] values 
(
	@Name,
	@CID
)

end

go

create procedure Appointments.addEmployee (@SSN as nvarchar(20), @FName as nvarchar(30), @MName as nvarchar(30), @LName as nvarchar(30), @AID as int,
@BDate as datetime, @PN as nvarchar(20), @DID as int, @EM as nvarchar(50), @NID as int, @Salary as decimal(8,2)) as 
begin

insert into Person.Employee values 
(
	@SSN,
	@FName,
	@MName,
	@LName,
	@AID,
	@BDate,
	@PN,
	@DID,
	@EM,
	@NID,
	getdate(),
	@Salary,
	default
)

end

go

create procedure Appointments.addPatient (@SSN as nvarchar(20), @FName as nvarchar(30), @MName as nvarchar(30), @LName as nvarchar(30), @AID as int,
@BDate as datetime, @PN as nvarchar(20),@EM as nvarchar(50)) as 
begin

insert into Person.Patient values 
(
	@SSN,
	@FName,
	@MName,
	@LName,
	@AID,
	@BDate,
	@PN,
	@EM
)
end

go

create procedure Appointments.addBrand (@Name as nvarchar(30),@AID as int,@PN as nvarchar(20),@EM as nvarchar(50)) as 
begin

insert into Stocks.Brand values
(
	@Name,
	@AID,
	@PN,
	@EM
)
end

go

create procedure Appointments.addDrug (@DTID as int, @BID as int, @SID as int, @EDate as datetime) as 
begin 

insert into Stocks.Drug values 
(
	@DTID,
	@BID,
	@SID,
	@EDate,
	getdate(),
	0
)

end

go

create procedure Appointments.addDrugType (@Name as nvarchar(30)) as
begin 

insert into Stocks.DrugType values 
(
	@Name
)

end

go

create procedure Appointments.addEquipment (@Name as nvarchar(30)) as
begin 

insert into Stocks.EquipmentType values
(
	@Name
)

end

go

create procedure Appointments.addMedical (@DID as int, @ETID as int, @BID as int, @SID as int, @EDate as datetime) as 
begin

insert into Stocks.MedicalEquipment values 
(
	@DID,
	@ETID,
	@BID,
	@SID,
	@EDate,
	GETDATE(),
	0
)

end

go

create procedure Appointments.addSupplier (@Name as nvarchar(30), @AID as int, @PN as nvarchar(20), @EM as nvarchar(30)) as 
begin

insert into Stocks.Supplier values
(
	@Name,
	@AID,
	@PN,
	@EM
)

end

go

/*create function selectAppointmentFunction ()
returns table ( AppointmentID as int, AppointmentDate as datetime, AppointmentFlag as bit, DepartmentID as int, 
EmployeeID as int, DiseaseName as nvarchar(30), PrescriptionID as int, PatientID as int, LocationID as int, Explanation as nvarchar(200)) as
begin
declare @result as table
update Appointments.Appointment set AppointmentFlag = 1 where AppointmentDate < GETDATE()
update Appointments.Appointment set AppointmentFlag = 0 where AppointmentDate >= GETDATE()
insert into @result
(
	select AppointmentID,AppointmentDate,AppointmentFlag,DepartmentID,EmployeeID,DiseaseName,PrescriptionID,PatientID,LocationID,Explanation from Appointments.Appointment
)
return (@result)

end
*/
go

create function Appointments.selectAppointmentFunction ()
returns @AppointmentsList Table (AppointmentID  int, AppointmentDate  datetime, AppointmentFlag  bit, DepartmentID  int, 
EmployeeID  int, DiseaseName  nvarchar(30), PrescriptionID  int, PatientID  int, LocationID  int, Explanation  nvarchar(200))
as 
begin

	    exec Appointments.selectAppointment 6
		--update Appointments.Appointment set Appointments.Appointment.AppointmentFlag = 1 where Appointments.Appointment.AppointmentDate < GETDATE()	
		--update Appointments.Appointment set Appointments.Appointment.AppointmentFlag = 0 where Appointments.Appointment.AppointmentDate >= GETDATE()
		return
	
end
go
--exec selectAppointment 6
go

exec Appointments.addAppointment '2017-12-10 15:12:13.999', 5, 5,3,2,'Something'

go
exec Appointments.addAppointment '2018-12-10 15:12:13.999', 4, 6,2,5,'Something'

go
exec Appointments.addAppointment '2017-12-10 15:12:13.999', 6, 7,1,4,'Something'

go
exec Appointments.addPrescriptions 2,'Apranax Augmentin', 8

go
exec Appointments.addDepartment 'Onuroloji', 2, '313-213-21-32', 9,2

go
exec Appointments.addSubstation 'Onuristan', 6, 9, '213-141-23-12'

go
exec Appointments.addExpense 'Onurun Maa��', 1.99, 5

go
exec Appointments.addIncome 'Onurun Maa� Kesintisi', 0.99, 5

go
exec Appointments.addTreatment 'Onur Tedavisi', 'Hastaya uygulanan y�ksek doz Onurin', 0.49,6

go
exec Appointments.addAddress 3,2,1,'Onur eczanesi kar��s�'

go
exec Appointments.addCity 'Onurk�y',3,2
 
go
exec Appointments.addCountry 'Onuronya','25012'

go
exec Appointments.addState 'Las Onur', 5

go
exec Appointments.addEmployee '12542315845', 'onuuur','onuuur','ooonur' , 5, '2016-12-16 17:18:10.000','212-213-12-32', 1, 'onurovic@dentalclinic.com', 9, 1.99

go
exec Appointments.addPatient '154785412', 'Onurovyum', 'Onurger', 'Onuro�lu', 6, '1993-12-16 17:18:10.000', '132-312-41-51', 'onurgil@dentalclinic.com'

go
exec Appointments.addBrand 'Onur A.�.', 7,'512-312-13-41','onurgida@dentalclinic.com'

go
exec Appointments.addDrug 2, 3, 1, '1990-12-16 17:18:10.000'

go
exec Appointments.addDrugType 'Geni� Spektrumlu Onurin'

go
exec Appointments.addEquipment '�ift kapakl� damar Onuru'

go
exec Appointments.addMedical 8, 2,3,4, '2020-12-16 17:18:10.000'

go
exec Appointments.addSupplier 'Onur Lojistik', 8, '313-213-12-32', 'onurlojistik@dentalclinic.com'

go

--exec selectAppointment 3

go

select * from Appointments.Appointment

select * from Appointments.Prescriptions

select * from Clinic.Department

select * from Clinic.Substation

select * from Financial.Expense

select * from Financial.Income

select * from Financial.Treatment

select * from Locations.[Address]

select * from Locations.City

select * from Locations.Country

select * from Locations.[State]

select * from Person.Employee

select * from Person.Patient

select * from Stocks.Brand

select * from Stocks.Drug

select * from Stocks.DrugType

select * from Stocks.EquipmentType

select * from Stocks.MedicalEquipment

select * from Stocks.Supplier

go

select e.EmployeeFirstName, e.EmployeeMiddleName, e.EmployeeLastName, d.DepartmentName
from Person.Employee as E join Clinic.Department as D
on e.EmployeeID = d.ManagerID

go
create trigger drug_delete on Stocks.Drug
instead of delete as
begin 
declare @delete_item int
select @delete_item= deleted.DrugID from deleted 
update Stocks.Drug set IsUsed=1 where DrugID=@delete_item

end
go

delete from Stocks.Drug where DrugID=10

go

create trigger employee_delete on Person.Employee
instead of delete as
begin
declare @delete_item int
select @delete_item = deleted.EmployeeID from deleted
update Person.Employee set IsWorking = 0 where EmployeeID = @delete_item 
end

go

delete from Person.Employee where EmployeeID = 5

go

create trigger delete_equipment on Stocks.MedicalEquipment
instead of delete as
begin
declare @delete_item int
select @delete_item = deleted.EquipmentTypeID from deleted
update Stocks.MedicalEquipment set IsUsed = 1 where EquipmentTypeID = @delete_item
end

go

delete from Stocks.MedicalEquipment where EquipmentTypeID = 7

go

create view select_addres as
select a.AddressID as AddressID, c.CountryName as Country, s.StateName as State, ci.CityName as City, a.FullAddress as [Full Address]
from Locations.[Address] as a join Locations.Country as c on a.CountryID = c.CountryID 
join Locations.[State] as s on a.StateID = s.StateID join Locations.City as ci on a.CityID = ci.CityID 

go

select * from select_addres

go

create view select_appointment as
select a.AppointmentID as ID, a.AppointmentFlag as Flag, a.AppointmentDate as [Date], concat(p.PatientFirstName,' ',p.PatientMiddleName, ' ',p.PatientLastName) as [Patient Name],
concat(e.EmployeeFirstName,' ',e.EmployeeMiddleName , ' ',e.EmployeeLastName) as [Doctor Name], d.DepartmentName as Department, s.SubstationName as Substation
from Appointments.Appointment as a join Person.Patient as p on a.PatientID = p.PatientID join Clinic.Department as d on a.DepartmentID = d.DepartmentID  
join Clinic.Substation as s on d.SubstationID = s.SubstationID join Person.Employee as e on e.EmployeeID = a.EmployeeID

go

select * from select_appointment
go

create view select_equipment as
select m.EquipmentID as ID, m.IsUsed as [Is Used], m.ExpDate as [Expiration Date], m.AcquisitionDate as [Acquisition Date], d.DepartmentName as [Department Name],e.EquipmentName as [Equipment Name],
b.BrandName as [Brand Name], s.SupplierName as [Supplier Name] 
from Stocks.MedicalEquipment as m join Stocks.EquipmentType as e on m.EquipmentTypeID = e.EquipmentTypeID join Clinic.Department as d on m.DepartmentID = d.DepartmentID 
join Stocks.Supplier as s on m.SupplierID = s.SupplierID join Stocks.Brand as b on m.BrandID = b.BrandID

go

select * from select_equipment
go

create view select_drug as
select d.DrugID as ID, d.IsUsed as [Is Used], d.ExpDate as [Expiration Date], d.AcquisitionDate as [Acquisition Date], 
t.DrugName as [Drug Name], b.BrandName as [Brand Name], s.SupplierName as [Supplier Name]
from Stocks.Drug as d join Stocks.DrugType as t on d.DrugTypeID = t.DrugTypeID join Stocks.Brand as b on d.BrandID = b.BrandID join Stocks.Supplier as s on d.SupplierID = s.SupplierID
go

select * from select_drug
go

create view select_financial_expense as 
select e.Quantity as [Quantity], e.[Date] as [Date], d.DepartmentName as [Department Name], s.SubstationName as [Substation Name], e.Explanation as [Explanation]
from Financial.Expense as e join Clinic.Department as d on e.DeparmentID = d.DepartmentID join Clinic.Substation as s on d.SubstationID = s.SubstationID 
go

select * from select_financial_expense 

go

create view select_financial_income as 
select e.Quantity as [Quantity], e.[Date] as [Date], d.DepartmentName as [Department Name], s.SubstationName as [Substation Name], e.Explanation as [Explanation]
from Financial.Income as e join Clinic.Department as d on e.DeparmentID = d.DepartmentID join Clinic.Substation as s on d.SubstationID = s.SubstationID 
go

select * from select_financial_income

go
update Person.Employee set DepartmentID=1 where EmployeeFirstName ='Hilal'

go
select * from Appointments.Appointment

select * from Person.Patient

select * from Person.Employee

select * from Stocks.Drug

go
